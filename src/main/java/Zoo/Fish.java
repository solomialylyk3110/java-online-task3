package Zoo;

public class Fish extends Animal {
    int countFins;
    public Fish(String color, int age, int countFins) {
        super(color, age);
        this.countFins=countFins;
    }
    public void swim(){
        System.out.println("I can swim");
    }
}

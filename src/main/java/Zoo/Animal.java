package Zoo;

public class Animal {
    String color;
    int age;

    public Animal(String color, int age) {
        this.color = color;
        this.age = age;
    }

    public void sleep() {
        System.out.println("Sleep");
    }
    public void eat() {
        System.out.println("Eat Anything");
    }
}

package Zoo;

public class Eagle extends Bird {
    public Eagle(String color, int age) {
        super(color, age);
    }

    @Override
    public void eat() {
        System.out.println("I eat Meat");
    }
}

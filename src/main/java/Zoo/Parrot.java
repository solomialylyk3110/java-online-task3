package Zoo;

public class Parrot extends Bird {
    public Parrot(String color, int age) {
        super(color, age);
    }

    @Override
    public void eat() {
        System.out.println("I eat Plants");
    }
}

package Zoo;

public class Bird extends Animal{

    public Bird(String color, int age) {
        super(color, age);
    }
    public void fly() {
        System.out.println("I can fly");
    }
}

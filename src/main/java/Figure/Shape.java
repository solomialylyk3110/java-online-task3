package Figure;

public abstract class Shape {

    String color;

    public Shape(String color) {
        this.color = color;
    }
}


import Figure.Circle;
import Figure.Rectangle;
import Figure.Shape;
import Figure.Triangle;
import Zoo.Animal;
import Zoo.Bird;
import Zoo.Fish;
import Zoo.Parrot;


public class Application{
    public static void main(String[] args) {
        Fish fish = new Fish("gold", 1, 2);
        fish.swim();
        Bird bird = new Parrot("green", 3);
        bird.fly();
        bird.eat();

        Shape circle1 = new Circle("blue");
        Shape circle2 = new Circle("red");
        Shape circle3 = new Circle("green");
        Shape rectangle1 = new Rectangle("blue");
        Shape rectangle2 = new Rectangle("red");
        Shape rectangle3 = new Rectangle("yellow");
        Shape triangle1 = new Triangle("orange");
        Shape triangle2 = new Triangle("purple");
        Shape triangle3 = new Triangle("grey");





    }
}
